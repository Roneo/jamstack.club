---
title: Hugo Cards
github: https://github.com/bul-ikana/hugo-cards
demo: https://hugo-cards-site.netlify.app/
author: Hugo Aguirre
ssg:
  - Hugo
cms:
  - No Cms
date: 2018-09-29T23:11:06.000Z
description: A bootstrap based minimal hugo theme based on webjeda-cards
stale: true
disabled: false
disabled_reason: ''
---
