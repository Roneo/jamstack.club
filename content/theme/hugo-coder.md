---
title: Coder
github: https://github.com/luizdepra/hugo-coder
demo: https://hugo-coder.netlify.app/
author: Luiz F. A. de Prá
ssg:
  - Hugo
cms:
  - No CMS
css:
  - SCSS
date: 2018-02-17T13:45:54.000Z
description: A minimalist blog theme for hugo.
stale: false
disabled: false
disabled_reason: ''
---
