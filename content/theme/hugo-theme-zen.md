---
title: Zen
github: https://github.com/frjo/hugo-theme-zen
demo: https://zen-demo.xdeb.org/
author: Fredrik Jonsson
ssg:
  - Hugo
cms:
  - No Cms
date: 2017-03-09T13:05:40.000Z
description: A fast and clean Hugo theme with css-grid and Hugo pipes support.
stale: false
disabled: false
disabled_reason: ''
---
