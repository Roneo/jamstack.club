---
title: Capsule
github: https://github.com/sudorook/capsule
demo: https://sudorook.gitlab.io/capsule-demo/
author: sudorook
ssg:
  - Hugo
cms:
  - No Cms
css:
  - Bulma
date: 2017-09-30T15:29:45.000Z
description: A Hugo theme based on the CSS-only Bulma framework.
stale: false
disabled: false
disabled_reason: ''
---
