---
title: Cactus Plus
github: https://github.com/nodejh/hugo-theme-cactus-plus
demo: https://nodejh.com/hugo-theme-mini/
author: Hang Jiang
ssg:
  - Hugo
cms:
  - No Cms
date: 2017-01-15T14:56:47.000Z
description: A minimalistic hugo theme based on cactus
stale: false
disabled: false
disabled_reason: ''
---
