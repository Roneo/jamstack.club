---
title: Goa
github: https://github.com/kaapiandcode/hugo-goa
demo: https://kaapiandcode.github.io/hugo-goa-demo/
author: Rajesh Shenoy
ssg:
  - Hugo
cms:
  - No Cms
date: 2016-10-08T02:59:24.000Z
description: Simple Minimalistic Theme for Hugo
stale: false
disabled: false
disabled_reason: ''
---
