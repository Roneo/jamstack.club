---
title: Arberia Theme for Hugo
github: https://github.com/antedoro/arberia
demo: https://arberiatheme.netlify.app/
author: V. Antedoro
author_link: https://antedoro.it
date: 2022-10-31T00:00:00.000Z
ssg:
  - Hugo
cms:
  - No CMS
css:
  - Bootstrap
archetype:
  - Blog
  - Landing Page
  - Portfolio
description: >-
  An Hugo theme for tech video blogger with 4 single post layout and 2 list
  layout, fully responsive and optimized!
stale: false
---

## Arberia Theme for Hugo is a bootstrap theme for tech video blogger, fully responsive and optimized

## Features

- Responsive layout
- 3 Single post view:
  - Post view with sidebar
  - Post view without sidebar
  - Post view with cover image
  - Video post view
- 2 List post view:
  - List posts
  - Grid posts
- Landing page
- List Cathegory/Tag view
- Icon colored submenu
- Table of Contents
- Search supported by [Flexbox.js](flexbox.js)
- [Google Analytics](https://analytics.google.com/analytics) supported
- [Disqus](https://disqus.com) comment system
- Social-Media Share buttons on posts
- Multilingual support. (not yet implemented!)
- Uses Hugo's asset generator with pipelining, fingerprinting, bundling and minification by default (No webpack, nodejs and other dependencies are required to edit the theme.


Key takeaways: this theme is recommended for tech blogger

