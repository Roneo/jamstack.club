---
title: Syna
github: https://github.com/okkur/syna
demo: https://about.okkur.org/syna/fragments/
author: Okkur Labs
ssg:
  - Hugo
cms:
  - No Cms
date: 2017-09-07T11:51:44.000Z
description: Highly customizable open source theme for Hugo based static websites
stale: true
disabled: false
disabled_reason: ''
---
